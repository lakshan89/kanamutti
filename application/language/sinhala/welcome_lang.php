<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['welcome_button'] = 'fydhuq';
$lang['term_title'] = 'fldkafoais';
$lang['term_para1'] = 'fuu ;r&Otilde;h wfma%,a ui 04 &Egrave;k isg uehs ui 04 jkod olajd l%shd;aul f&otilde;&#39 by&lt;u ,l=Kq ,nd .kak ;r&Otilde;lrejka yg j&aacute;kd ;Hd. ,ndfokq we;&#39';
$lang['term_para2'] = 'Tn &uacute;iska imhkq ,nk f;dr;=re i;H tajd &uacute;h hq;= w;r&quot; h&iuml;lsis f;dr;=rla wi;H tlla nj Tmamq jqjfyd;a&quot; mQ&frac34;j oekq&iuml;&sect;ulska f;drj tlS ;r&Otilde;lrejd fyda ;r&Otilde;ld&szlig;h ;r&Otilde;hg kqiq&yuml;iafil= f,i i,lkq ,f&iacute;&#39';
$lang['term_para3'] = 'ch.%dylhskaf.a k&iuml; uehs ui 15 &Egrave;k ;=,&sect; m%ldYhg m;a lrkq ,ef&iacute;&#39 ;Hd. ,nd&sect;u&quot; ch.%dylhska m%ldYhg m;al&lt; &Egrave;k isg i;s 3 - 4 ld,h we;=,; is&yuml;lrkq ,ef&iacute;&#39';
$lang['term_para4'] = 'wod, ;Hd. ksl=;a lrkq ,nkafka fkd&#39 48$7&quot; rcu,aj;a; mdr&quot; n;a;ruq,a, hk ,smskfhys we;s fid*a&Uuml;flda&acirc;b&Uuml; fid&uml;Ikaia ^mqoa&amp; iud.u  fj;sks&#39';
$lang['term_agree_button'] = 'tlÕhs';
$lang['register_your_details'] = 'Tnf.a úia;r';
$lang['register_name'] = 'ku';
$lang['register_email'] = 'B-fï,a';
$lang['register_phone'] = 'ÿrl:k wxlh';
$lang['register_submit'] = 'fhduq lrkak';
$lang['game_howtoPlay'] = 'fi,a,ï lrkafk fldfyduo@';
$lang['game_score'] = ',l=Kq';
$lang['score_timesup'] = "bjrd''hsææ";
$lang['score_scored'] = ",l=Kq";
$lang['score_points'] = "Points";
$lang['score_done'] = "bjrhs";
$lang['score_replay'] = "kej;;a";
$lang['how_play'] = "fi,a,u lrkafk fufyuhs";
$lang['how_para1'] = "fldlsia wvx.= rEm u; la,sla lrkak'";
$lang['how_para2'] = "iEu ksjer&Egrave; la,sla ls&Iacute;ulgu ,l=Kq 100la ,efnk w;r&quot; jer&Egrave; la,sla ls&Iacute;ul&sect; ,l=Kq 50la wvq jkq we;'";
$lang['how_para3'] = "ch.%yKh i|yd&quot; ;;amr 45la ;=,&sect; .;yels Wm&szlig;u ,l=Kq m%udKh ,nd.; hq;=f&otilde;'";
$lang['how_para4'] = "by&lt;u ,l=Kq ,enQjka i|yd ;Hd. ,ndfokq we;'";
$lang['how_back'] = "wdmiq";
$lang['thanks_wish'] = "iqN w¨;a wjqreoaola fõjd'''æ";
