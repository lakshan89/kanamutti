<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['welcome_button'] = 'தொடக்கம் ';
$lang['term_title'] = 'விதிமுறைகளும் மற்றும் நிபந்தனைகளும்';
$lang['term_para1'] = 'இப்போட்டியானது 04-04-2017 முதல் 04-05-2017 வரை நடைபெறும்.';
$lang['term_para2'] = 'இப்போட்டியில் பங்குபற்றுவதற்கு முன்னர் அனைத்து பேட்டியாளர்களும் தமது முக்கியமான தகவல்களை வழங்குமாறு கேட்டுகொள்ள படுகிறீர்கள்.';
$lang['term_para3'] = 'தவறான மற்றும் பிழையான தகவல்களை வழங்கினால் உடனடியாக பேட்டியில் இருந்து நீக்கப்படுவீர்கள். முதல் 100 இடங்களை கைப்பற்றும் பேட்டியாளர்களும், போட்டி ஏட்பாட்டாளர்களிடமிருந்து தகுந்த பரிசுகளை பெறுவதற்கான தகுதியை பெறுவீர்கள்';
$lang['term_para4'] = 'இப்போட்டியில் பங்குபற்றியவர்கள் தமது பரிசுகளை தகுந்த முன் அறிவித்தலுடன் "48/7 இரஜமல்வத்தை வீதி, பத்தரமுல்லை, இலங்கை. " என்ற முகவரிக்கு நேரில் வந்து பெற்றுக்கொள்ளலாம்.';
$lang['term_agree_button'] = 'ஏற்கிறேன்';
$lang['register_your_details'] = 'உங்களது விபரங்கள்';
$lang['register_name'] = 'பெயர்';
$lang['register_email'] = 'மின்னஞ்சல்';
$lang['register_phone'] = 'தொடர்பு எண்';
$lang['register_submit'] = 'சமர்ப்பி';
$lang['game_howtoPlay'] = 'எப்படி விளையாடுவது';
$lang['game_score'] = 'மதிப்பெண்';
$lang['score_timesup'] = "நேரம் முடிந்தது";
$lang['score_scored'] = "அடித்தார்";
$lang['score_points'] = "புள்ளிகள்";
$lang['score_done'] = "செய்து";
$lang['score_replay'] = "மறுபடியும்";
$lang['how_play'] = "எப்படி விளையாடுவது";
$lang['how_para1'] = "'Play' ஐ கிளிக் செய்து ஆரம்பிக்கவும். குக்கீஸ் காணப்படும் படங்களை கிளிக் செய்யவும்";
$lang['how_para2'] = "ஒவ்வொரு சரியான கிளிக்குற்கு 100 புள்ளிகள் வழங்கப்படும், ஒவ்வொரு பிழையான கிளிக்குற்கு 50 புள்ளிகள் குறைக்கப்படும்";
$lang['how_para3'] = "45 செக்கன்களில் முடிந்தவரையான புள்ளிகளை பெற முயற்சிக்கவும்.";
$lang['how_para4'] = "உங்களது மிகச்சிறந்த புள்ளியை உங்கள் நண்பர்களுடன் 'Share' செய்து அவர்களுக்கு சவால் விடுக்கவும். அதிகூடிய புள்ளிகளை பெறுவொர் பரிசு பெறுவீர்கள்.";
$lang['how_back'] = "மீண்டும்";
$lang['thanks_wish'] = "இனிய புத்தாண்டு வாழ்த்துக்கள்..!!!";