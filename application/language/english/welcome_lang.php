<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['welcome_button'] = 'Start';
$lang['term_title'] = 'Terms & Conditions';
$lang['term_para1'] = 'This competition will run from the 4th of April to 4th of May 2017. All participants taking part in this competition are required to submit some basic information before playing. ';
$lang['term_para2'] = 'Failure to provide accurate information or providing of false information would result in immediate disqualification.';
$lang['term_para3'] = 'The winners of the competition will be announced on the 15th of May 2017. Prize issuing will be done within 3-4 weeks of a time period from the date of winner announcement.';
$lang['term_para4'] = 'Participants could collect their gifts from 48/7, Rajamalwatte Road, Battaramulla, Sri Lanka with prior notice of arrival.';
$lang['term_agree_button'] = 'Agree';
$lang['register_your_details'] = 'Your Details';
$lang['register_name'] = 'Name';
$lang['register_email'] = 'Email';
$lang['register_phone'] = 'Phone Number';
$lang['register_submit'] = 'Submit';
$lang['game_howtoPlay'] = 'How To Play';
$lang['game_score'] = 'Score';
$lang['score_timesup'] = "Time's up";
$lang['score_scored'] = "Scored";
$lang['score_points'] = "Points";
$lang['score_done'] = "Done";
$lang['score_replay'] = "Replay";
$lang['how_play'] = "How To Play";
$lang['how_para1'] = "Click 'PLAY NOW' to begin and click on the images that has the Kokis";
$lang['how_para2'] = "You score 100 points for each correct click and you lose 50 points for the wrong click.";
$lang['how_para3'] = "Score as much as you can within 45 seconds to win.";
$lang['how_para4'] = "Share your best score with your friends and challenge them! and The Top scorers win Gifts!";
$lang['how_back'] = "Back";
$lang['thanks_wish'] = "Wish you a Happy New Year..!!!";
