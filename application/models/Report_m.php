<?php  

defined('BASEPATH') OR exit('No direct script access allowed');

class Report_m extends MY_Model {

	public $_table 		= 'GameSessions';
	public $primary_key = 'id';

	public function get_scores()
	{
		$this->db->select('g.score, g.attempts, g.correct, g.wrong, g.start, g.end, ( g.end - g.start) AS duration,  g.logged_device, g.logged_browser, p.name, p.email, p.contact');
		$this->db->from('GameSessions g');
		$this->db->join('Participants p', 'p.id = g.participent', 'left');
		return $this->db->get()->result();
	}

	public function total_users()
	{
		$this->db->from('Participants');
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function total_sessions()
	{
		$this->db->from('GameSessions');
		$query = $this->db->get();
		return $query->num_rows();
	}

}

/* End of file Participant_m.php */
/* Location: ./application/models/Participant_m.php */