<?php  

defined('BASEPATH') OR exit('No direct script access allowed');

class Notify_m extends MY_Model {

	public $_table 		= 'ContactEmail';
	public $primary_key = 'id';

	/**
	 * Insert a participant and returns the insert ID
	 * @param  [array] $post array that contains the user information
	 * @return [int]  		 Last Insert id
	 */
	public function _insert($email)
	{
		
		$row = array(
			'id' 		=> '',
			'email' 	=> $email
		);

		return $this->insert($row);
	}
}

/* End of file Participant_m.php */
/* Location: ./application/models/Participant_m.php */