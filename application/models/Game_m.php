<?php  

defined('BASEPATH') OR exit('No direct script access allowed');

class Game_m extends MY_Model {

	public $_table 		= 'GameSessions';
	public $primary_key = 'id';

	public function _insert($key)
	{
		$row = array(
			'id' 			=> '',
			'key' 			=> $key,
			'participent'  	=> $this->session->userdata('id'),
			'score' 		=> 0,
			'correct' 		=> 0,
			'wrong' 		=> 0,
			'start' 		=> time(),
			'end' 			=> 0,
			'logged_device' => $this->session->userdata('device_os').'/'.$this->session->userdata('user_device'),
			'logged_browser'=> $this->session->userdata('user_browser')
		);

		return $this->insert($row);
	}

	public function _update($post)
	{
		$row = array(
			'score' 		=> $post['score'],
			'correct' 		=> $post['correct'],
			'wrong' 		=> $post['wrong'],
			'attempts' 		=> $post['attempts'],
			'end' 			=> time(),
			'logged_device' => $this->session->userdata('device_os').'/'.$this->session->userdata('user_device'),
			'logged_browser'=> $this->session->userdata('user_browser')
		);

		return $this->update($this->session->userdata('game_id'), $row);
	}

	public function get_score($key)
	{
		$this->db->select('*');
		$this->db->from('GameSessions');
		$this->db->join('Participants', 'GameSessions.participent = Participants.id', 'left');
		$this->db->where('GameSessions.key', $key);
		return $this->db->get()->row();
	}

	public function get_leader_board()
	{
		$this->db->select('GameSessions.score,(GameSessions.end - GameSessions.start) AS duration, GameSessions.attempts, GameSessions.correct,GameSessions.wrong,GameSessions.START,GameSessions.END,Participants.*');
		$this->db->from('GameSessions');
		$this->db->join('Participants', 'GameSessions.participent = Participants.id', 'left');
		$this->db->where('GameSessions.score !=', 0);
		$this->db->group_by('name');
		$this->db->order_by('GameSessions.score', 'DESC');
		$this->db->limit(10);

		return $this->db->get()->result();
	}
	

}

/* End of file Participant_m.php */
/* Location: ./application/models/Participant_m.php */