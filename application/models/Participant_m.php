<?php  

defined('BASEPATH') OR exit('No direct script access allowed');

class Participant_m extends MY_Model {

	public $_table 		= 'Participants';
	public $primary_key = 'id';

	/**
	 * Insert a participant and returns the insert ID
	 * @param  [array] $post array that contains the user information
	 * @return [int]  		 Last Insert id
	 */
	public function _insert($post)
	{
		$row = array(
			'id' 		=> '',
			'name'  	=> $post['name'],
			'contact' 	=> $post['contact'],
			'email' 	=> $post['email'],
			'nic' 		=> "",
			'address' 	=> "",
			'timestamp' => time()
		);

		return $this->insert($row);
	}

	public function _set_session($id)
	{

		$usr = $this->get($id);

		$user = array(
	        'id'  		=> $usr->id,
	        'name'  	=> $usr->name,
	        'type'     	=> 'PAR',
	        'logged_in' => TRUE
		);

		$this->session->set_userdata($user);
	}

	public function _set_cookie($id)
	{

		$this->load->helper('cookie');

		$this->load->library('encryption');
		$this->encryption->initialize(
	        array(
                'cipher' => 'aes-256',
                'mode' => 'ctr',
                'key' => '<a 32-character random string>'
	        )
		);

		$cookie = array(
			'name'   => 'logged_in',
		    'value'  => $this->encryption->encrypt($id),
		    'expire' => '86500',
		    'domain' => $_SERVER['SERVER_NAME'],
		    'prefix' => '_'
		);

		set_cookie($cookie);
		
	}
	

}

/* End of file Participant_m.php */
/* Location: ./application/models/Participant_m.php */