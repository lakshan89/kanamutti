<?php $this->load->view('inc/public_header.php'); ?>

<section class="main">
	
	<?php ($_SERVER["REQUEST_URI"] == "/game" )? $cls="game_container" : $cls ="";  ?>

	<?php if($this->uri->segment(2) != "reports"):  $cont = "container"; else: $cont = "container-fluid"; endif; ?>

		<div class="<?php echo $cont.' '.$cls; ?>">
			<?php  if(isset($subView) && $subView!=""): $this->load->view($subView); endif; ?>
		</div>	
		
</section>

<?php $this->load->view('inc/public_footer.php'); ?>