<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/ico" href="<?php echo site_url(); ?>resources/images/fav.ico"/>
	<title>කෝ කොකිස් | By iPlayDeal</title>

	<meta property="og:url"           content="http://kokis.iplaydeal.lk/" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="කොකිස් හොයමු. තෑගි දිනමු!" />
	<meta property="og:description"   content="Find as many Kokis as you can within 45 seconds and stand a chance to win exciting prizes this Avurudu season!" />
	<meta property="og:image"         content="<?php echo site_url(); ?>resources/images/fb_share_v2.png" />

	<!-- STYLES -->
	<link rel="stylesheet" href="<?php echo site_url(); ?>bower_components/normalize-css/normalize.css">
	<link rel="stylesheet" href="<?php echo site_url(); ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo site_url(); ?>bower_components/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo site_url(); ?>resources/css/styles.css">
	
	<?php if($this->uri->segment(2) != "reports"): ?>

	<link href="https://fonts.googleapis.com/css?family=Open+Sans|Rancho" rel="stylesheet">

	<?php endif; ?>

	
	<?php if($this->uri->segment(2) == "reports"): ?>

		<script src="<?php echo site_url(); ?>bower_components/jquery/dist/jquery.min.js"></script>
		<link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
		<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>

	<?php else: ?>

		<!-- SCRIPTS -->
		<script src="<?php echo site_url(); ?>bower_components/jquery/dist/jquery.min.js"></script>
		<script src="<?php echo site_url(); ?>bower_components/crypto-js/crypto-js.js"></script>
		<script src="<?php echo site_url(); ?>bower_components/jquery-validation/dist/jquery.validate.min.js"></script>		
		<script src="<?php echo site_url(); ?>resources/js/scripts.js"></script>

	<?php endif; ?>

</head>
<body>
	
