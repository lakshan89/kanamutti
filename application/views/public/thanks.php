<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8&appId=224526077606186";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="page thanks text-center inner">


	<div class="thanks__branding">
		<a href="http://iplaydeal.lk/" class="ipd">Powered By iPlayDeal</a>
	</div>


	<div class="thanks_msg">
		<div>

			<h3><?php echo $this->lang->line('thanks_wish'); ?></h3>
			<a href="https://www.facebook.com/iPlayDeal/" target="_blank">facebook.com/iPlayDeal</a>

		</div>

		<div class="fb-like" data-href="https://www.facebook.com/iPlayDeal/" data-layout="button" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
	</div>

</div>
