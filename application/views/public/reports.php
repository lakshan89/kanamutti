<div class="row">
	<div class="col-md-12">

		<br>
		<br>
		
		<pre>Total Users : <?php echo $users; ?><br>Total Sessions : <?php echo $sessions; ?></pre>

		<br>
		<br>

		
		<table class="table table-bordered table-striped" id="scores">

		 <thead>
			  <tr>
			  	<th>Score</th>
				<th>Attempts</th>
				<th>Correct</th>
				<th>Wrong</th>
				<th>Start</th>
				<th>End</th>
				<th>Duration</th>
				<th>Status</th>
				<th>OS/Logged Device</th>
				<th>Browser</th>
				<th>Name</th>
				<th>Email</th>
				<th>Contact</th>
			  </tr>
		 </thead>

		  <tbody>
			 
			<?php foreach ($rows as $row) { ?>
				<tr>
			  		<td><?php echo $row->score ?></td>
				  	<td><?php echo $row->attempts ?></td>
				  	<td><?php echo $row->correct ?></td>
				  	<td><?php echo $row->wrong ?></td>
				  	<td><?php echo date("Y-m-d H:i:s", $row->start) ?></td>
				  	<td><?php echo date("Y-m-d H:i:s", $row->end) ?></td>
				  	<td>
						<?php if( $row->duration > 0){ echo $row->duration.'s'; }else{ echo '--'; } ?>				  		
				  	</td>
					<td>
						<?php $scoreCheck = ($row->correct * 100) - ($row->wrong * 50); ?>
						<?php if($row->duration > 50): ?>
							<span class="label label-danger">Cheated</span>
						<?php elseif( $row->attempts > 100 ): ?>
							<span class="label label-danger">Cheated</span>
						<?php elseif( $row->attempts >= 60 ): ?>
							<span class="label label-warning">Suspicious</span>
						<?php elseif( $scoreCheck != $row->score ): ?>
							<span class="label label-warning">Suspicious</span>
						<?php else: ?>
							<span class="label label-success">Verified</span>
						<?php endif; ?>


					</td>

				  	<td><?php echo $row->logged_device ?></td>
				  	<td><?php echo $row->logged_browser ?></td>
				  	<td><?php echo $row->name ?></td>
				  	<td><a href="mailto:<?php echo $row->email ?>" target="_blank"><?php echo $row->email ?></a></td>
				  	<td><a href="tel:<?php echo $row->contact ?>"><?php echo $row->contact ?></a></td>
		  		</tr>
			<?php } ?>		  	
		  	
		  </tbody>
		</table>

	</div>
</div>

<script>
	jQuery(document).ready(function($) {
		$('#scores').DataTable({
			"pageLength": 100,
			"order": [[ 0, "desc" ]]
		});
	});
</script>