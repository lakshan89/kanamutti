<div class="page">
	<div class="container">
		
		<div class="content game">
			<div class="clearfix">
				<div class="game__info">
						<div class="game__how">
							<a href="<?php echo site_url('how') ?>">How to Play??</a>
						</div>
						<div class="game__score">Score <span>0</span></div>
				</div>
				<div class="game__timmer">45</div>
			</div>

			<div class="game__board">
				<div class="game__preloader">
					<a href="#" class="play_btn" id="start_game">
						<img src="<?php echo site_url(); ?>resources/images/game/play_btn.png" alt="">
					</a>
				</div>

				<div class="clearfix game__blocks">
					<a href="#" class="game__tile swing"></a>
					<a href="#" class="game__tile swing"></a>
					<a href="#" class="game__tile swing"></a>
					<a href="#" class="game__tile swing"></a>
					<a href="#" class="game__tile swing"></a>
					<a href="#" class="game__tile swing"></a>
				</div>

				<div class="game__blocker"></div>

				<input type="hidden" name="game_key" id="game_key" value="<?php echo $game_key; ?>">
				<!-- <div class="game__end"><p>Times Up!!</p></div> -->
			</div>

		</div>
	</div>
</div>