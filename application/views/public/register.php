<div class="page">
	<div class="container">
			
			<div class="row">
				<div class="col-md-4 col-md-offset-4">

					<form method="POST" class="form-horizontal" id="register-form" action="<?php echo site_url("register/validate") ?>" novalidate>
					
						<div class="form-group">
							<label for="name">Name</label>
							<input id="name" type="text" name="name" class="form-control" required>
							<?php echo form_error('name'); ?>
						</div>

						<div class="form-group">
							<label for="email">Email</label>
							<input id="email" type="email" name="email" class="form-control"  required>
							<?php echo form_error('email'); ?>
						</div>

						<div class="form-group">
							<label for="tel">Telephone</label>
							<input id="contact" type="tel" name="contact" class="form-control"  required>
							<?php echo form_error('contact'); ?>
						</div>

						<div class="form-group">
							<input type="submit" name="submit" class="btn btn-primary" value="Register" class="inner_btn submit_btn">
						</div>

					</form>
				</div>
			</div>

	</div>
</div>
