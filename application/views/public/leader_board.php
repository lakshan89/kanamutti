
<h1>Leader Board</h1>
<table class="table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Score</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($leaders as $leader): ?>
            <tr>
                <td><?php echo $leader->name ?></td>
                <td><?php echo $leader->score ?></td>
            </tr>
        <?php endforeach ?>
    </tbody>
</table>