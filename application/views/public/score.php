<div class="page inner fr-lyr">
	<div class="page--bg"></div>

	<a href="http://iplaydeal.lk/" class="ipd">Powered By iPlayDeal</a>


	<div class="content thanks">

		<div class="thanks__content">
			<h1 class="text-center">
				<!-- <div class="score_title">
					<h3><?php echo $this->lang->line('score_timesup'); ?></h3>
				</div> -->
			</h1>
			<div class="score__card">

				<p class="text-center score__text">
					<span class="player-name"><?php
					$username = $score->name;
					$arr = explode(' ',trim($username));
					 echo $arr[0]; ?></span> scored<br>
					<strong><?php echo $score->score ?></strong><br>
					<small>points</small>
				</p>
			</div>
			
		</div>

	</div>

<div class="btn-wrapper">
	<a href="#" class="shareBtn" id="shareBtn">
		<img src="<?php echo site_url(); ?>resources/images/share.png" alt="">
	</a>
</div>
<div class="btn-wrapper">
	<a href="<?php echo site_url('game/thanks') ?>" class="done_btn">
		<?php echo $this->lang->line('score_done'); ?>
	</a>
	<a href="<?php echo site_url('game') ?>" class="reply_btn">
		<?php echo $this->lang->line('score_replay'); ?>
	</a>
</div>



</div>


<script>

	window.fbAsyncInit = function() {

	    FB.init({
	      appId      : 198396903982015, // App ID
	      status     : true,    // check login status
	      cookie     : true,    // enable cookies to allow the
	                            // server to access the session
	      xfbml      : true,     // parse page for xfbml or html5
	                            // social plugins like login button below
	      version     : 'v2.7',  // Specify an API version
	    });

    // Put additional init code here
  };

  // Load the SDK Asynchronously
	(function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
	     js.src = "//connect.facebook.net/en_US/sdk.js";
	     fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

  document.getElementById('shareBtn').onclick = function() {
	  FB.ui({
	  method: 'feed',
	  link: 'http://kokis.iplaydeal.lk/',
	  image : '<?php echo site_url(); ?>resources/images/fb_share_v2.png',
	  title : '<?php echo $score->name ?> has Scored <?php echo $score->score ?> Points in Ko Kokis Game. ',
	  description: "Join now and beat <?php echo $score->name ?>'s Score and Win Prizes...",
	  caption: 'Ko Kokis App',
		}, function(response){});
	}

	jQuery(document).ready(function($) {
		window.onbeforeunload = function() {$(location).attr('href', '/game/');   };
	});



</script>
