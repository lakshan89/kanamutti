<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		$this->load->model('participant_m','Model');
	}

	/**
	 * Load Register Page
	 */
	public function index()
	{

		// Check if the user is a returning user.
		$this->_isReturningUser();		

		$data['subView']= "public/register";
		$this->load->view('public_template',$data);
	}

	/**
	 * Validate the Register  form and save data to the Database
	 */
	public function validate()
	{
		$this->load->library('form_validation');

		$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');
		$this->form_validation->set_rules('contact', 'Contact Number', 'required');
		

		if ($this->form_validation->run() == FALSE) {

            $this->index();
        
        }else {

        	// check if these user exists.
        	$user_exsist = $this->Model->get_by(array(
        		'email' => $this->input->post('email'),
        		'contact'   => $this->input->post('contact')
        	));

        	if(isset($user_exsist->id) && $user_exsist->id != ""):

        		$id = $user_exsist->id;

        	else:

        		$id = $this->Model->_insert($this->input->post());
        		if($id == 0 && $id == ""): $this->index(); endif;

        	endif;

        	$this->_set_session($id);
        }

	}

	private function _isReturningUser()
	{

		// is user has a session or a cookie
		if($this->session->userdata('logged_in')) : redirect('/game','refresh'); endif;

		// $this->load->helper('cookie');
		// if(get_cookie('_logged_in') != NULL) {

		// 	$this->load->library('encryption');
		// 	$this->encryption->initialize(
		//         array(
		//             'cipher' => 'aes-256',
		//             'mode' => 'ctr',
		//             'key' => '<a 32-character random string>'
		//         )
		// 	);

		// 	$id = $this->encryption->decrypt(get_cookie('_logged_in'));

		// 	$this->Model->_set_session($id);
		// 	redirect('/game','refresh');
		// }

	}

	private function _set_session($id)
	{
        if($id != 0 && $id != ""):

        	// Set Session 
        	$this->Model->_set_session($id);

         	// Set Cookies
        	// $this->Model->_set_cookie($id);

        	// Send the user to the game
        	redirect('/game','refresh');

        endif;
	}
}
