<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Game extends CI_Controller {

	/**
	 * Load Game Page
	 */
	public function index()
	{
		$this->load->library('user_agent');
		$data['device_os'] = $this->agent->platform();	
		$data['user_browser'] = $this->agent->browser();
		$data['user_device'] = $this->agent->mobile();	
		//$this->session->sess_destroy();
		if(!$this->session->userdata('logged_in')) : redirect('/','refresh'); endif;

		// Initialize game session and session key
		$data['game_key'] = md5($this->session->userdata('id').'-'.time().'-'.rand());

		$this->session->set_userdata(  array(
			'game_key' => $data['game_key'],
			'device_os'=> $data['device_os'],
			'user_browser'=> $data['user_browser'],
			'user_device'=> $data['user_device']
		) );

		 $correctImages = scandir(FCPATH."/resources/images/foods/correct");
		 $incorrectImages = scandir(FCPATH."/resources/images/foods/incorrect");
		 $correct_images = [];
		 $incorrect_images = [];
		 $images = [];
		 foreach($correctImages as $correct) {
			 if($correct == "." || $correct == ".."){
				 continue;
			 }
			$img_path = FCPATH."/resources/images/foods/correct/" . $correct;
			$correct_images[] = $img_path;
		 }
		 foreach($incorrectImages as $incorrect) {
			 	if($incorrect == "." || $incorrect == ".."){
					continue;
				}
				$img_path = FCPATH."/resources/images/foods/incorrect/" . $incorrect;
			 	$correct_images[] = $img_path;
		 }
		$data['correct_images'] = $correct_images;
		$data['incorrect_images'] = $incorrect_images;
		$data['subView']= "public/game";
		$this->load->view('public_template',$data);
	}

	private function getImageData($path){
		
		// Read image path, convert to base64 encoding
		$imageData = base64_encode(file_get_contents($path));

		// Format the image SRC:  data:{mime};base64,{data};
		return 'data: '.mime_content_type($path).';base64,'.$imageData;
	}

	/**
	 * Configure the Game.
	 * Crate all the combinations and send as a one encrypted package.
	 */
	public function end($key)
	{

		$this->load->model('game_m','Model');
		$data['score']	= $this->Model->get_score($key);
		$data['key'] 	= $key;

		$data['subView']= "public/score";
		$this->load->view('public_template',$data);
	}

	public function share($key)
	{

		$this->load->model('game_m','Model');
		$data['score'] = $this->Model->get_score($key);
		$data['key']   = $key;

		$data['subView']= "public/score";
		$this->load->view('public_template',$data);
	}

	/**
	 * Configure the Game.
	 * Crate all the combinations and send as a one encrypted package.
	 */
	public function thanks()
	{
		$this->load->helper('cookie');
		delete_cookie('_logged_in');
		$this->session->sess_destroy();
		

		$data['subView']= "public/thanks";
		$this->load->view('public_template',$data);
	}


	public function log_game_start()
	{
		if($this->session->userdata('game_key') == $_POST['key'] ) {

			$this->load->model('game_m','Model');
			$id = $this->Model->_insert($this->session->userdata('game_key'));
			$ary = array(
				'game_id' => $id
			);

			$this->session->set_userdata( $ary );

			echo json_encode(array('status' => true, 'msg' =>'Game Init Successful.' ));

		}else{
			echo json_encode(array('status' => false, 'msg' =>'Invalid Key' ));
		}


	}

	public function log_game_end()
	{
		if($this->session->userdata('game_key') == $_POST['result']['key'] ) {

			// check the validity of the results sent.
			$post = $_POST['result'];
			$flag = md5($post['score'].'-'.$post['correct'].'-'.$post['wrong'].'-'.$post['attempts']);

			if($flag == $_POST['flag']) {

				$this->session->set_userdata( array( 'score' => $post['score']) );

				$this->load->model('game_m','Model');
				$this->Model->_update($_POST['result']);

				echo json_encode(array('status' => true, 'msg' =>'Game End Successful.' ));

			}else{

				echo json_encode(array('status' => true, 'msg' =>'Invalid Data'));

			}


		}else{
			echo json_encode(array('status' => false, 'msg' =>'Invalid Key' ));
		}
	}
	public function leader_board()
	{
		$this->load->model('game_m','Model');
		$res = $this->Model->get_leader_board();
		$data['subView']= "public/leader_board";
		$data['leaders'] = $res;
		$this->load->view('public_template',$data);
	}
}
