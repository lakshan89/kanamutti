<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notify extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here

		$this->load->model('Notify_m','Model');
	}

	/**
	 * Load Register Page
	 */
	public function index()
	{
		// check if these user exists.
		$user_exsist = $this->Model->get_by(array(
			'email' => $_GET['email']
		));

		if(isset($user_exsist->id) && $user_exsist->id != ""):

			$id = $user_exsist->id;
			$status = array('status' => 'TRUE', 'msg' =>'User Exist.' );
			echo json_encode($status);


		else:
			$id = $this->Model->_insert($_GET['email']);
			$status = array('status' => 'TRUE', 'msg' =>'User Added' );
			echo json_encode($status);

		endif;

	}
}
