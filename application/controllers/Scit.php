<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scit extends CI_Controller {

	/**
	 * Load Welcome Page
	 */
	public function index()
	{

		//$this->session->set_userdata(array('site_lang'=>'sinhala'));

		$data['subView']= "public/welcome";
		$this->load->view('public_template',$data);
	}

	/**
	 * Load Terms and Condition Page
	 */
	public function reports($pwd)
	{

		if ($pwd != "E22craNatre") {
			redirect('/','refresh');
		}

		$this->load->model('Report_m','Model');

		$data['users'] 		= $this->Model->total_users();
		$data['sessions'] 	= $this->Model->total_sessions(); 
		$data['rows'] 		= $this->Model->get_scores();
		
		$data['subView']= "public/reports";
		$this->load->view('public_template',$data);
	}
}
