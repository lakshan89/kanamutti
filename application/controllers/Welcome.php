<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Load Welcome Page
	 */
	public function index()
	{

		//$this->session->set_userdata(array('site_lang'=>'sinhala'));

		$data['subView']= "public/welcome";
		$this->load->view('public_template',$data);
	}

	/**
	 * Load Terms and Condition Page
	 */
	public function terms()
	{
		$data['subView']= "public/terms";
		$this->load->view('public_template',$data);
	}

	/**
	 * Load How to Play Page
	 */
	public function how()
	{
		$data['subView']= "public/how";
		$this->load->view('public_template',$data);
	}
}
