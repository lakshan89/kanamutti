jQuery(document).ready(function($) {
	var KokisGame = {

		images : [
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/1.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/10.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/11.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/13.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/14.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/16.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/17.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/2.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/4.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/5.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/6.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/8.jpg'
		],

		correctImages : [
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/12.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/15.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/18.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/3.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/7.jpg',
			window.location.protocol + '//' + window.location.host + '/resources/images/foods/9.jpg'
		],


		init: function () {

			// Bind Events
			this.eventBindings();
			this.game_key = $('#game_key').val();
			this.result.key = this.game_key;
		},

		eventBindings : function () {

			var self = this;

			// Game Init Click
			jQuery('#start_game').click(function(event) {
				event.preventDefault();
				$('.game__preloader').fadeOut('400');
				self.startGame();
			});

			jQuery('.game__tile').click(function(event) {
				event.preventDefault();

				$('.game__blocker').show();

				if(self.isGameActive) {

					combination = self.generateCombinations();
					self.checkScore(this);

					setTimeout(function(){  self.setStage(combination);  }, 250);
				}

			}).dblclick(function(e) {
				console.log("double-clicked but did nothing");
				e.stopPropagation();
				e.preventDefault();
				return false;
			});

		},

		startGame : function() {
			var self 		= this,
				combination = "";

			// Start Counter
			self.startTimer();

			self.result.attempts 	 = 0;
			self.result.correct 	 = 0;
			self.result.wrong 	 	 = 0;

			// Get Combination
			combination = self.generateCombinations();
			self.setStage(combination);
		},

		startTimer : function () {
			var self = this;
			self.isGameActive = true;

			var display = $('.game__timmer');

			var timer = self.duration, minutes, seconds;

			self.result.start = self.get_timestamp();

			self.log_start();
		    self.timmerInstance = setInterval(function () {
		        minutes = parseInt(timer / 60, 10);
		        seconds = parseInt(timer % 60, 10);

		        minutes = minutes < 10 ? "0" + minutes : minutes;
		        seconds = seconds < 10 ? "0" + seconds : seconds;

		        // display.text(minutes + ":" + seconds);
		        display.text(seconds);

		        if (--timer < 0) {

		            self.endGame();

		        }

		    }, 1000);

		},

		endGame : function () {

			var self = this;

			self.isGameActive   = false;
			self.result.end 	= self.get_timestamp();
			self.result.score   = self.score;
			self.log_end();

			clearInterval(self.timmerInstance);
			$('.game__end').fadeIn('400');

			setTimeout(function(){ $(location).attr('href', '/game/end/'+self.game_key); }, 1000);

		},

		generateCombinations : function () {

			var self 	 = this,
				selected = [],
				temp 	 = [],
				item  	 = "",
				i        = 0;

			// Select Random Correct Image
			item = self.correctImages[Math.floor(Math.random()*self.correctImages.length)];
			temp.push(item);
			selected.push({ 'img' : item, 'flag' : true });

			// Select Random Incorrect Images
			while (i < 5) {
				item = self.images[Math.floor(Math.random()*self.images.length)];

				if(temp.indexOf(item) == -1) {
					temp.push(item);
					selected.push({ 'img' : item, 'flag' : false });
					i++;
				}
			}

			return self.shuffleArray(selected);

		},

		shuffleArray : function (array) {
			var currentIndex = array.length, temporaryValue, randomIndex;

			// While there remain elements to shuffle...
			while (0 !== currentIndex) {

				// Pick a remaining element...
				randomIndex = Math.floor(Math.random() * currentIndex);
				currentIndex -= 1;

				// And swap it with the current element.
				temporaryValue = array[currentIndex];
				array[currentIndex] = array[randomIndex];
				array[randomIndex] = temporaryValue;
			}

			return array;
		},

		setStage : function(data){
			var self = this;
			this.can_images = [];
			var i = 0;
			$('.game .game__tile').each(function(index, el){
				self.can_images[i] = new Image();
				var data_url = '';

				self.can_images[i].setAttribute('crossOrigin', 'anonymous');

				self.can_images[i].onload = function () {
					var canvas = document.createElement("canvas");
					canvas.width =this.width;
					canvas.height =this.height;

					var ctx = canvas.getContext("2d");
					ctx.drawImage(this, 0, 0);

					var dataURL = canvas.toDataURL("image/png");


						$(el)
							.css('background-image', 'url(' + dataURL + ')')
							.removeClass('correct error')
							.data('flag',data[index].flag);
				};

				self.can_images[i].src = data[i].img;
				i++;
			});

			$('.game__blocker').hide();
		},

		checkScore : function(el) {
			var self = this;

			// Check to see if the game active
			if(self.isGameActive) {

				self.result.attempts = self.result.attempts + 1;

				if($(el).data('flag') == true){

					self.score 			= self.score + 100;
					self.result.correct = self.result.correct + 1;
					$(el).addClass('correct');


				}else{

					self.score 			= self.score - 50;
					self.result.wrong 	= self.result.wrong + 1;
					$(el).addClass('error');
					if(self.score < 0){ self.score = 0 }

				}

				$('.game__score span').text(self.score);

			}
		},

		log_start : function(){

			self = this;

			self.send_to_server({
				url: '/game/log_game_start',
				type: 'POST',
				dataType: 'json',
				data: {'key': self.game_key },
			});

		},

		log_end : function(){

			self = this;
			self.send_to_server({
				url: '/game/log_game_end',
				type: 'POST',
				dataType: 'json',
				data: {'result': self.result, 'flag' : self.create_flag() },
			});


		},

		score : 0,
		time :  0,
		timmerInstance : null,
		duration : (60 / 4)*3,
		isGameActive : false,
		game_key : "",
		result : {},

		send_to_server : function($args) {

			$.ajax($args)
			.done(function(data) {
				if(!data.status){
					alert('Ooopz.. Something went Wrong! Please Refresh the Page and Try Again...');
				}
			})
			.fail(function() {
				alert('Ooopz.. Something went Wrong! Please Refresh the Page and Try Again...');
			});
		},

		get_timestamp : function() {
			var d = new Date();
			return d.getTime();
		},

		create_flag : function () {
			var self = this;
			return  CryptoJS.MD5(self.result.score+'-'+self.result.correct+'-'+self.result.wrong+'-'+self.result.attempts).toString();
		}

	};


    KokisGame.init();

});
